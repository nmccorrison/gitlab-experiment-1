# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Configuration do
  it "responds to the expected getter/setter interface" do
    expect((described_class.methods - Object.methods).sort).to eq [
      :name_prefix, :name_prefix=,
      :logger, :logger=,
      :base_class, :base_class=,
      :cache, :cache=,
      :default_rollout, :default_rollout=,
      :context_key_secret, :context_key_secret=,
      :context_key_bit_length, :context_key_bit_length=,
      :mount_at, :mount_at=,

      :redirect_url_validator, :redirect_url_validator=,
      :inclusion_resolver, :inclusion_resolver=,
      :tracking_behavior, :tracking_behavior=,
      :publishing_behavior, :publishing_behavior=,
      :cookie_domain, :cookie_domain=,

      :context_hash_strategy=, # deprecated in 0.6.0
      :variant_resolver, :variant_resolver=, # deprecated in 0.5.0

      :instance, :_load # singleton assurances
    ].sort
  end

  it "has deprecation warnings for context_hash_strategy" do
    expect(ActiveSupport::Deprecation).to receive(:warn).twice

    block = ->(*args) { args.join('|') }
    config.context_hash_strategy = block

    expect(config.instance_variable_get(:@__context_hash_strategy)).to eq(block)
    expect(experiment(:example).key_for('foo')).to eq('foo|gitlab_experiment_example')

    config.context_hash_strategy = nil
  end

  it "has deprecation warnings for variant_resolver" do
    expect(ActiveSupport::Deprecation).to receive(:warn).twice
    original = config.inclusion_resolver
    updated = -> {}

    expect(config.variant_resolver).to eq(original)

    config.variant_resolver = updated

    expect(config.inclusion_resolver).to eq(updated)

    config.inclusion_resolver = original
  end
end
