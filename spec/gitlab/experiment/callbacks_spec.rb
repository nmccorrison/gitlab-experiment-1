# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Callbacks do
  subject(:subject_experiment) { Gitlab::Experiment.new(:example) }

  after do
    subject_experiment.class.reset_callbacks(:unsegmented)
    subject_experiment.class.reset_callbacks(:segmentation_check)
    subject_experiment.class.reset_callbacks(:exclusion_check)
  end

  it "defines the expected callbacks" do
    expect(subject_experiment.class.__callbacks).to include(:unsegmented)
    expect(subject_experiment.class.__callbacks).to include(:segmentation_check)
    expect(subject_experiment.class.__callbacks).to include(:exclusion_check)
  end

  describe ".exclude" do
    it "adds the filters to the correct callback chain" do
      subject_experiment.class.exclude { false }
      subject_experiment.class.exclude { true }
      subject_experiment.class.exclude { raise(:not_expected) } # this will never get called because we abort

      expect(subject_experiment.run_callbacks(:exclusion_check)).to be_falsey
    end

    it "raises an exception when there are no filters" do
      expect { subject_experiment.class.exclude }.to raise_error(
        ArgumentError,
        'no filters provided'
      )
    end
  end

  describe ".segment" do
    it "adds the filters to the correct callback chain" do
      subject_experiment.class.segment(variant: '_bar_') { false }
      subject_experiment.class.segment(variant: '_foo_') { true }
      subject_experiment.class.segment(variant: '_baz_') { true } # this will never get called because we terminate

      subject_experiment.run_callbacks(:segmentation_check)

      expect(subject_experiment.variant.name).to eq('_foo_')
    end

    it "raises an exception when there are no filters" do
      expect { subject_experiment.class.segment(variant: :variant) }.to raise_error(
        ArgumentError,
        'no filters provided'
      )
    end
  end
end
