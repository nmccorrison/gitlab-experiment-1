# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Rollout::Random do
  subject(:subject_experiment) { Gitlab::Experiment.new(:example) }

  before do
    stub_experiments(example: true)
    subject_experiment.rollout(described_class)
  end

  it "returns whatever random result sample pulls out" do
    expect(subject_experiment.variant_names).to receive(:sample).and_return(:variant2)

    subject_experiment.try(:variant1) {}
    subject_experiment.try(:variant2) {}

    expect(subject_experiment.variant.name).to eq('variant2')
  end
end
