# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Gitlab::Experiment::Rollout do
  it "resolves a rollout by symbol or string name" do
    expect(described_class.resolve(:random)).to be(described_class::Random)
    expect(described_class.resolve('round_robin')).to be(described_class::RoundRobin)
  end

  describe Gitlab::Experiment::Rollout::Base do
    subject(:subject_experiment) { Gitlab::Experiment.new(:example) }

    before do
      stub_experiments(example: true)

      subject_experiment.rollout(described_class)
      subject_experiment.try(:variant1) {}
      subject_experiment.try(:variant2) {}
    end

    it "delegates methods to the experiment" do
      expect(subject.variant_names).to eq([:variant1, :variant2])
      expect(subject.cache).to be(subject_experiment.cache)
      expect(subject.cache).to be_an_instance_of(Gitlab::Experiment::Cache::Interface)
      expect(subject.id).to eq(subject_experiment.id)
    end

    it "validates when executing the rollout strategy" do
      expect_any_instance_of(described_class).to receive(:validate!)
      expect_any_instance_of(described_class).to receive(:execute)

      subject_experiment.variant.name
    end

    it "returns the first variant" do
      expect(subject_experiment.variant.name).to eq('variant1')
    end
  end
end
