# frozen_string_literal: true

require 'spec_helper'
require 'rspec-parameterized'

RSpec.describe Gitlab::Danger::Helper do
  using RSpec::Parameterized::TableSyntax

  class FakeDanger
    include Gitlab::Danger::Helper

    attr_reader :git, :gitlab

    def initialize(git:, gitlab:)
      @git = git
      @gitlab = gitlab
    end
  end

  let(:fake_git) { double('fake-git') }

  let(:mr_author) { nil }
  let(:fake_gitlab) { double('fake-gitlab', mr_author: mr_author) }

  subject(:helper) { FakeDanger.new(git: fake_git, gitlab: fake_gitlab) }

  describe '#gitlab_helper' do
    context 'when gitlab helper is not available' do
      let(:fake_gitlab) { nil }

      it 'returns nil' do
        expect(helper.gitlab_helper).to be_nil
      end
    end

    context 'when gitlab helper is available' do
      class InvalidFakeDanger
        include Gitlab::Danger::Helper
      end

      subject(:helper) { InvalidFakeDanger.new }

      it 'returns nil' do
        expect(helper.gitlab_helper).to be_nil
      end
    end
  end

  describe '#release_automation?' do
    context 'when gitlab helper is not available' do
      it 'returns false' do
        expect(helper.release_automation?).to be_falsey
      end
    end

    context 'when gitlab helper is available' do
      context "but the MR author isn't the RELEASE_TOOLS_BOT" do
        let(:mr_author) { 'johnmarston' }

        it 'returns false' do
          expect(helper.release_automation?).to be_falsey
        end
      end

      context 'and the MR author is the RELEASE_TOOLS_BOT' do
        let(:mr_author) { described_class::RELEASE_TOOLS_BOT }

        it 'returns true' do
          expect(helper.release_automation?).to be_truthy
        end
      end
    end
  end

  describe '#all_changed_files' do
    subject { helper.all_changed_files }

    it 'interprets a list of changes from the danger git plugin' do
      expect(fake_git).to receive(:added_files) { %w[a b c.old] }
      expect(fake_git).to receive(:modified_files) { %w[d e] }
      expect(fake_git)
        .to receive(:renamed_files)
        .at_least(:once)
        .and_return([{ before: 'c.old', after: 'c.new' }])

      is_expected.to contain_exactly('a', 'b', 'c.new', 'd', 'e')
    end
  end

  describe '#project_name' do
    subject { helper.project_name }

    it { is_expected.to eq('gitlab-experiment') }
  end

  describe '#markdown_list' do
    it 'creates a markdown list of items' do
      items = %w[a b]

      expect(helper.markdown_list(items)).to eq("* `a`\n* `b`")
    end

    it 'wraps items in <details> when there are more than 10 items' do
      items = ('a'..'k').to_a

      expect(helper.markdown_list(items)).to match(%r{<details>[^<]+</details>})
    end
  end

  describe '#changes_by_category' do
    it 'categorizes changed files' do
      expect(fake_git).to receive(:added_files).and_return(%w[foo foo.md foo.rb foo.js db/migrate/foo])
      allow(fake_git).to receive(:modified_files).and_return([])
      allow(fake_git).to receive(:renamed_files).and_return([])

      expect(helper.changes_by_category).to eq(
        backend: %w[foo.md foo.rb],
        database: %w[db/migrate/foo],
        frontend: %w[foo.js],
        unknown: %w[foo]
      )
    end
  end

  describe '#category_for_file' do
    where(:path, :expected_category) do
      'doc/foo'         | :none
      'CONTRIBUTING.md' | :backend
      'LICENSE'         | :backend
      'MAINTENANCE.md'  | :backend
      'PHILOSOPHY.md'   | :backend
      'PROCESS.md'      | :backend
      'README.md'       | :backend

      'app/assets/foo'       | :frontend
      'app/views/foo'        | :frontend
      'public/foo'           | :frontend
      'scripts/frontend/foo' | :frontend
      'spec/javascripts/foo' | :frontend
      'spec/frontend/bar'    | :frontend
      'vendor/assets/foo'    | :frontend
      'jest.config.js'       | :frontend
      'package.json'         | :frontend
      'yarn.lock'            | :frontend

      'app/models/foo' | :backend
      'bin/foo'        | :backend
      'config/foo'     | :backend
      'lib/foo'        | :backend
      'rubocop/foo'    | :backend
      'spec/foo'       | :backend
      'spec/foo/bar'   | :backend

      'generator_templates/foo' | :backend
      'vendor/languages.yml'    | :backend
      'vendor/licenses.csv'     | :backend

      'Gemfile'        | :backend
      'Gemfile.lock'   | :backend
      'Procfile'       | :backend
      'Rakefile'       | :backend
      'FOO_VERSION'    | :backend

      'Dangerfile'                                            | :backend
      'danger/commit_messages/Dangerfile'                     | :backend
      'danger/commit_messages/'                               | :backend
      '.gitlab-ci.yml'                                        | :backend
      '.gitlab/ci/cng.gitlab-ci.yml'                          | :backend
      '.gitlab/ci/ee-specific-checks.gitlab-ci.yml'           | :backend
      'scripts/foo'                                           | :backend
      'lib/gitlab/danger/foo'                                 | :backend
      '.overcommit.yml.example'                               | :backend
      '.codeclimate.yml'                                      | :backend
      '.editorconfig'                                         | :backend

      'lib/gitlab/ci/templates/Security/SAST.gitlab-ci.yml'   | :backend

      'spec/features/feature_spec.rb' | :test

      'qa/page/login.rb' | :qa

      'db/schema.rb'                                              | :database
      'db/migrate/foo'                                            | :database
      'db/post_migrate/foo'                                       | :database
      'lib/gitlab/background_migration.rb'                        | :database
      'lib/gitlab/background_migration/foo'                       | :database
      'lib/gitlab/database.rb'                                    | :database
      'lib/gitlab/database/foo'                                   | :database
      'lib/gitlab/sql/foo'                                        | :database
      'rubocop/cop/migration/foo'                                 | :database

      'db/fixtures/foo.rb'                                        | :backend

      'FOO'          | :unknown
      'foo'          | :unknown

      'foo/bar.rb'  | :backend
      'foo/bar.js'  | :frontend
      'foo/bar.txt' | :backend
      'foo/bar.md'  | :backend
    end

    with_them do
      subject { helper.category_for_file(path) }

      it { is_expected.to eq(expected_category) }
    end
  end

  describe '#label_for_category' do
    where(:category, :expected_label) do
      :backend   | '~backend'
      :database  | '~database'
      :docs      | '~documentation'
      :foo       | '~foo'
      :frontend  | '~frontend'
      :none      | ''
      :qa        | '~QA'
    end

    with_them do
      subject { helper.label_for_category(category) }

      it { is_expected.to eq(expected_label) }
    end
  end

  describe '#new_teammates' do
    it 'returns an array of Teammate' do
      usernames = %w[filipa iamphil]

      teammates = helper.new_teammates(usernames)

      expect(teammates.map(&:username)).to eq(usernames)
    end
  end

  describe '#missing_database_labels' do
    subject { helper.missing_database_labels(current_mr_labels) }

    context 'when current merge request has ~database::review pending' do
      let(:current_mr_labels) { ['database::review pending', 'feature'] }

      it { is_expected.to match_array(['database']) }
    end

    context 'when current merge request does not have ~database::review pending' do
      let(:current_mr_labels) { ['feature'] }

      it { is_expected.to match_array(['database', 'database::review pending']) }
    end
  end
end
