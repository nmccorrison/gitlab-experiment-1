# frozen_string_literal: true

module Gitlab
  class Experiment
    module Rollout
      class Random < Base
        # Pick a random variant if we're in the experiment group. It doesn't
        # take into account small sample sizes but is useful and performant.
        def execute
          variant_names.sample
        end
      end
    end
  end
end
