# frozen_string_literal: true

module Gitlab
  class Experiment
    Error = Class.new(StandardError)
    InvalidRolloutRules = Class.new(Error)
  end
end
