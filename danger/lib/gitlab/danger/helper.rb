# frozen_string_literal: true

require_relative 'teammate'

module Gitlab
  module Danger
    module Helper
      RELEASE_TOOLS_BOT = 'gitlab-release-tools-bot'

      # Returns a list of all files that have been added, modified or renamed.
      # `git.modified_files` might contain paths that already have been renamed,
      # so we need to remove them from the list.
      #
      # Considering these changes:
      #
      # - A new_file.rb
      # - D deleted_file.rb
      # - M modified_file.rb
      # - R renamed_file_before.rb -> renamed_file_after.rb
      #
      # it will return
      # ```
      # [ 'new_file.rb', 'modified_file.rb', 'renamed_file_after.rb' ]
      # ```
      #
      # @return [Array<String>]
      def all_changed_files
        Set.new
          .merge(git.added_files.to_a)
          .merge(git.modified_files.to_a)
          .merge(git.renamed_files.map { |x| x[:after] })
          .subtract(git.renamed_files.map { |x| x[:before] })
          .to_a
          .sort
      end

      def gitlab_helper
        # Unfortunately the following does not work:
        # - respond_to?(:gitlab)
        # - respond_to?(:gitlab, true)
        gitlab
      rescue NameError
        nil
      end

      def release_automation?
        gitlab_helper&.mr_author == RELEASE_TOOLS_BOT
      end

      def project_name
        'gitlab-experiment'
      end

      def markdown_list(items)
        list = items.map { |item| "* `#{item}`" }.join("\n")

        if items.size > 10
          "\n<details>\n\n#{list}\n\n</details>\n"
        else
          list
        end
      end

      # @return [Hash<String,Array<String>>]
      def changes_by_category
        all_changed_files.each_with_object(Hash.new { |h, k| h[k] = [] }) do |file, hash|
          hash[category_for_file(file)] << file
        end
      end

      # Determines the category a file is in, e.g., `:frontend` or `:backend`
      # @return[Symbol]
      def category_for_file(file)
        _, category = CATEGORIES.find { |regexp, _| regexp.match?(file) }

        category || :unknown
      end

      # Returns the GFM for a category label, making its best guess if it's not
      # a category we know about.
      #
      # @return[String]
      def label_for_category(category)
        CATEGORY_LABELS.fetch(category, "~#{category}")
      end

      CATEGORY_LABELS = {
        docs: "~documentation", # Docs are reviewed along DevOps stages, so don't need roulette for now.
        none: "",
        qa: "~QA",
        test: "~test ~Quality for `spec/features/*`"
      }.freeze
      CATEGORIES = {
        %r{\Adoc/} => :none, # To reinstate roulette for documentation, set to `:docs`.
        %r{\A(CONTRIBUTING|LICENSE|MAINTENANCE|PHILOSOPHY|PROCESS|README)(\.md)?\z} => :backend,
        %r{\Aapp/(assets|views)/} => :frontend,
        %r{\Apublic/} => :frontend,
        %r{\Aspec/(javascripts|frontend)/} => :frontend,
        %r{\Avendor/assets/} => :frontend,
        %r{\Ascripts/frontend/} => :frontend,
        %r{(\A|/)(
          \.babelrc |
          \.eslintignore |
          \.eslintrc(\.yml)? |
          \.nvmrc |
          \.prettierignore |
          \.prettierrc |
          \.scss-lint.yml |
          \.stylelintrc |
          \.haml-lint.yml |
          \.haml-lint_todo.yml |
          babel\.config\.js |
          jest\.config\.js |
          karma\.config\.js |
          webpack\.config\.js |
          package\.json |
          yarn\.lock |
          \.gitlab/ci/frontend\.gitlab-ci\.yml
        )\z}x => :frontend,

        %r{\Adb/(?!fixtures)[^/]+} => :database,
        %r{\Alib/gitlab/(database|background_migration|sql)(/|\.rb)} => :database,
        %r{\Arubocop/cop/migration(/|\.rb)} => :database,

        %r{\A(\.gitlab-ci\.yml\z|\.gitlab\/ci)} => :backend,
        %r{\A\.editorconfig\z} => :backend,
        %r{Dangerfile\z} => :backend,
        %r{\A(danger/|lib/gitlab/danger/)} => :backend,
        %r{\Ascripts/} => :backend,
        %r{\A\.overcommit\.yml\.example\z} => :backend,
        %r{\A\.codeclimate\.yml\z} => :backend,

        %r{\Aapp/(?!assets|views)[^/]+} => :backend,
        %r{\A(bin|config|generator_templates|lib|rubocop)/} => :backend,
        %r{\Aspec/features/} => :test,
        %r{\Aspec/(?!javascripts|frontend)[^/]+} => :backend,
        %r{\Avendor/(?!assets)[^/]+} => :backend,
        %r{\Avendor/(languages\.yml|licenses\.csv)\z} => :backend,
        %r{\A(Gemfile|Gemfile.lock|Procfile|Rakefile)\z} => :backend,
        %r{\A[A-Z_]+_VERSION\z} => :backend,
        %r{\A\.rubocop(_todo)?\.yml\z} => :backend,
        %r{\A\.tool-versions\z} => :backend,

        %r{\Aqa/} => :qa,

        # Fallbacks in case the above patterns miss anything
        %r{\.(rb|ru)\z} => :backend,
        %r{(
          \.(md|txt)\z |
          \.markdownlint\.json
        )}x => :backend,
        %r{\.js\z} => :frontend
      }.freeze

      def new_teammates(usernames)
        usernames.map { |u| Gitlab::Danger::Teammate.new('username' => u) }
      end

      def missing_database_labels(current_mr_labels)
        labels = if has_database_scoped_labels?(current_mr_labels)
                   ['database']
                 else
                   ['database', 'database::review pending']
                 end

        labels - current_mr_labels
      end

      private

      def has_database_scoped_labels?(current_mr_labels)
        current_mr_labels.any? { |label| label.start_with?('database::') }
      end
    end
  end
end
