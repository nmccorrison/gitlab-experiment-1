# frozen_string_literal: true

require 'bundler/gem_tasks'
Rake::Task['release'].clear # remove the standard release task -- not usable and may be confusing

require 'rspec/core/rake_task'
RSpec::Core::RakeTask.new(:spec) do |t|
  t.exclude_pattern = 'spec/{danger}/**/*_spec.rb'
end

namespace :spec do
  desc 'Run the danger code examples'
  RSpec::Core::RakeTask.new(:danger) do |t|
    t.pattern = 'spec/danger/**/*_spec.rb'

    ENV['SIMPLE_COV_FILTERS'] = '/experiment' # ignore our experiment project files
    Rake::Task['spec:danger'].invoke # force invoking it now!
  end

  RSpec::Core::RakeTask.new(:without_rails) do |t|
    t.exclude_pattern = 'spec/{danger}/**/*_spec.rb'

    ENV['INCLUDE_RAILS'] = 'false'
    Rake::Task['spec:without_rails'].invoke # force invoking it now!
  end
end

task default: :spec
